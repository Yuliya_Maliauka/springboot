<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Topic</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 <fmt:setLocale value="en_US" scope="session"/>
    <div class="onli">  
    <table style="width: 100%">
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>

			<div class="topic-info-container">		        
		        <ul> <li><h1>${postInfo.postName}</h1></li>
		        </ul>
		        <ul>
		            <li><h2>Author: ${postInfo.postUser}</h2></li>
		            <li><h2> <fmt:formatDate value="${postInfo.postDate}" pattern="dd-MM-yyyy HH:mm"/></h2></li>
		            <li><p align="justify">${postInfo.postDescription}</p></li>		            
		        </ul>
		 
		        <c:forEach items="${paginationResult}" var="users">
		        
		        <c:if test="${users.userName eq pageContext.request.userPrincipal.name}">
		        <form action="${pageContext.request.contextPath}/newComment" method="get">
		        	<input  type="hidden" name="id" value="${postInfo.id}" />
		        	<input  type="hidden" name="idTopic" value="${postInfo.topicID}" />
					<textarea name="description" cols="40" rows="2"></textarea>
					<br><input name="submit" type="submit" value="Add comment" />
					<br>
				</form>
				</c:if>
				
		      	</c:forEach>
		        <div style="width: 100%">
		         <br>		        
			    <c:forEach items="${commentDetails}" var="commentsInfo">
			     	<c:if test="${postInfo.id eq commentsInfo.postId}">	
				     		<c:if test="${commentsInfo.ID_comment eq commentsInfo.ID_coomentChild}">					     		 		     							     	        
							        <div class="level${commentsInfo.levelChild}" style="width: 50%">								    	
									            Author: ${commentsInfo.userNameChild}, <fmt:formatDate value="${commentsInfo.creteDateChild}" pattern="dd-MM-yyyy HH:mm"/>
									            <br>
									            ${commentsInfo.descriptionChild}
									            <br>
									            
									 <c:if test="${postInfo.postUser eq pageContext.request.userPrincipal.name}">
									 <a style="color: red;" href="${pageContext.request.contextPath}/removeComment?cod=${commentsInfo.ID_comment}&id=${postInfo.id}&idTopic=${postInfo.topicID}">Delete</a>						 
									 </c:if> 
									 <c:if test="${commentsInfo.userNameChild eq pageContext.request.userPrincipal.name}">
									 <a style="color: red;" href="#">Edit</a>						 
									 </c:if>          								     
							        </div>	
							        <div style="margin-left:100px; width: 50%"> 
							        <c:forEach items="${paginationResult}" var="users">				        
		        					<c:if test="${users.userName eq pageContext.request.userPrincipal.name}">
								         <form action="${pageContext.request.contextPath}/answerComment" method="get">
								        	<input  type="hidden" name="id" value="${postInfo.id}" />
								        	<input  type="hidden" name="idTopic" value="${postInfo.topicID}" />
								        	<input  type="hidden" name="commentID" value="${commentsInfo.ID_comment}" />
											<textarea name="description" cols="40" rows="2"></textarea>
											<br><input name="submit" type="submit" value="Add comment" />
											<br>
										</form>
									</c:if>
									</c:forEach>
							        </div>								
							</c:if>
							<c:forEach items="${listChild}" var="listChildInfo">
					     		<c:if test="${listChildInfo.ID_comment eq commentsInfo.ID_coomentChild}">					     		 		     							     	        
								        <div class="level${listChildInfo.levelChild}" style="width: 50%">								    	
										            Author: ${listChildInfo.userNameChild}, <fmt:formatDate value="${listChildInfo.creteDateChild}" pattern="dd-MM-yyyy HH:mm"/>
										            <br>
										            ${listChildInfo.descriptionChild}
										             <br>
									 
									     <c:if test="${listChildInfo.levelChild eq 2}">
									     	 <c:if test="${postInfo.postUser eq pageContext.request.userPrincipal.name}">									 
												<a style="color: red;" href="${pageContext.request.contextPath}/removeComment?cod=${listChildInfo.ID_comment}&id=${postInfo.id}&idTopic=${postInfo.topicID}">Delete</a>
											 </c:if>
											 <c:if test="${commentsInfo.userNameChild eq pageContext.request.userPrincipal.name}">
											 	<a style="color: red;" href="#">Edit</a>						 
											 </c:if>
										 </c:if>									 
										 <c:if test="${listChildInfo.levelChild eq 3}">	
										 	 <c:if test="${postInfo.postUser eq pageContext.request.userPrincipal.name}">								 
												<a style="color: red;" href="${pageContext.request.contextPath}/removeComment?cod=${listChildInfo.ID_coomentChild}&id=${postInfo.id}&idTopic=${postInfo.topicID}">Delete</a>
											 </c:if>
										 </c:if>									 
									     								     
								        </div>	
								        <div style="margin-left:200px; width: 50%">
								        	<c:if test="${listChildInfo.levelChild eq 2}">
								        	<c:forEach items="${paginationResult}" var="users">				        
		        							<c:if test="${users.userName eq pageContext.request.userPrincipal.name}">
										        <form action="${pageContext.request.contextPath}/answerChild" method="get">
										        	<input  type="hidden" name="id" value="${postInfo.id}" />
										        	<input  type="hidden" name="idTopic" value="${postInfo.topicID}" />
										        	<input  type="hidden" name="commentID" value="${commentsInfo.ID_comment}" />
										        	<input  type="hidden" name="commentChildID" value="${listChildInfo.ID_comment}" />
													<textarea name="description" cols="40" rows="2"></textarea>
													<br><input name="submit" type="submit" value="Add comment" />
													<br>
												</form>
											</c:if>
											</c:forEach>
											</c:if>
								        </div>								    
							    </c:if>																        
			       			</c:forEach> 		    
						</c:if>							        
			       </c:forEach> 	
		     	</div>
			</div>			
			</td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>