package com.epam.thinkers.dao;

import java.util.List;

import com.epam.thinkers.entity.Notification;
import com.epam.thinkers.model.NotificationInfo;

public interface NotificationDAO {
	
public List<Notification> listNotification();

	public void remove(Notification name);
	public List<NotificationInfo> listNotificationInfo(String name);
	public Notification findNotificationByTopicNeme(String nameTopic);
	
	public Notification findNotificationByID(String id);

	public List<NotificationInfo> listmember(String topicName);
	public List<NotificationInfo> listaccount();
	
	public void saveNotification(NotificationInfo notification);
	public void updateNotification(NotificationInfo notificationInfo);
	public List<NotificationInfo> listNotificationInfoMy(String name);
	
	public NotificationInfo getNotificationInfo(String inviter, String nameTopic);
}
