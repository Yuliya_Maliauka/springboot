package com.epam.thinkers.dao;

import java.util.List;

import com.epam.thinkers.model.CommentDetailsInfo;
import com.epam.thinkers.model.CommentInfo;
import com.epam.thinkers.model.PaginationResult;

public interface CommentDAO {
	public List<CommentInfo> listCommentInfo();
	public List<CommentDetailsInfo> listCommentDetailInfo(Long idCom);
	public PaginationResult<CommentDetailsInfo> pagesCommentDetailInfo(int page, int maxResult, int maxNavigationPage, Long idCom);
	public void savecomment(CommentInfo commentInfo);
	public void saveAnswerComment(CommentInfo commentInfo);
	public void saveAnswerChild(CommentInfo commentInfo);
	public void remove(Long commentID);
}
