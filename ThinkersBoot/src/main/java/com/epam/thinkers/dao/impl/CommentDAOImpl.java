package com.epam.thinkers.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.thinkers.dao.CommentDAO;
import com.epam.thinkers.dao.PostDAO;
import com.epam.thinkers.dao.TopicDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Comment;
import com.epam.thinkers.entity.CommentDetails;
import com.epam.thinkers.entity.Notification;
import com.epam.thinkers.entity.Post;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.CommentDetailsInfo;
import com.epam.thinkers.model.CommentInfo;
import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.PostInfo;
import com.epam.thinkers.model.TopicInfo;

public class CommentDAOImpl implements CommentDAO {

	@Autowired
    private SessionFactory sessionFactory;
	
	@Autowired
    private PostDAO postDAO;
	
	public List<CommentInfo> listCommentInfo(){
		String sql = "Select new " + CommentInfo.class.getName() //
				+ "(c.ID, c.userName, c.description, c.creteDate, c.level, c.post.id) "//
                + " from " + Comment.class.getName() + " c";
        
		Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        //query.setParameter("id", postId);
 
        return query.list();
	}
	
	public List<CommentDetailsInfo> listCommentDetailInfo(Long idCom){
		String sql = "Select new " + CommentDetailsInfo.class.getName() //
				+ "(d.ID, c.post.id, d.comment.ID, d.coomentChild.ID, c.userName, c.description, c.creteDate, c.level, d.ID_Parent) "//
                + " from " + Comment.class.getName() + " as c join " + CommentDetails.class.getName() + " as d "
                + " on c.ID = d.coomentChild.ID where (d.comment.ID = :idCom) group by d.coomentChild.ID ORDER BY d.coomentChild.ID ASC";
        
		Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(sql);
        query.setParameter("idCom", idCom);
 
        return query.list();
	}
	
	public PaginationResult<CommentDetailsInfo> pagesCommentDetailInfo(int page, int maxResult, int maxNavigationPage, Long idCom){
		String sql = "Select new " + CommentDetailsInfo.class.getName() //
				+ "(d.ID, c.post.id, d.comment.ID, d.coomentChild.ID, c.userName, c.description, c.creteDate, c.level, d.ID_Parent) "//
                + " from " + Comment.class.getName() + " as c join " + CommentDetails.class.getName() + " as d "
                + " on c.ID = d.coomentChild.ID where (d.comment.ID = :idCom) group by d.coomentChild.ID ORDER BY d.coomentChild.ID ASC";
        
		Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(sql);
        query.setParameter("idCom", idCom);
        
        return new PaginationResult<CommentDetailsInfo>(query, page, maxResult, maxNavigationPage);
 
	}
	
	private Comment findComment(Long ID) {
		Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Comment.class);
        crit.add(Restrictions.eq("ID", ID));
        return (Comment) crit.uniqueResult();
	}
	
	public void savecomment(CommentInfo commentInfo){
		
		Long code = commentInfo.getID();
		String postID = commentInfo.getPostID();
		Post post = postDAO.findPost(postID);
        Comment comment = null;
        
        if (code != null) {
        	comment = this.findComment(code);
        }
        if (comment == null) {        	
            
        	comment = new Comment();
            
        	comment.setID(commentInfo.getID());
        	comment.setCreteDate(commentInfo.getCreteDate()); 
        	comment.setLevel(commentInfo.getLevel());
        	comment.setUserName(commentInfo.getUserName());
        	comment.setPost(post);        	
        }  
        comment.setDescription(commentInfo.getDescription());
        
        this.sessionFactory.getCurrentSession().persist(comment);
        
        	CommentDetails detail = new CommentDetails();
            detail.setID(UUID.randomUUID().toString());
            detail.setID_Parent(0);
            detail.setComment(comment);
            detail.setCoomentChild(comment);
            
            this.sessionFactory.getCurrentSession().persist(detail);           
        
	}

	public void saveAnswerComment(CommentInfo commentInfo){
		Long code = commentInfo.getID();
		String postID = commentInfo.getPostID();
		Long commentID = commentInfo.getCommentID();
		Post post = postDAO.findPost(postID);
        Comment comment = null;
        
        if (code != null) {
        	comment = this.findComment(code);
        }
        if (comment == null) {        	
            
        	comment = new Comment();
            
        	comment.setID(commentInfo.getID());
        	comment.setCreteDate(commentInfo.getCreteDate()); 
        	comment.setLevel(commentInfo.getLevel());
        	comment.setUserName(commentInfo.getUserName());
        	comment.setPost(post);        	
        }  
        comment.setDescription(commentInfo.getDescription());
        
        this.sessionFactory.getCurrentSession().persist(comment);
        
        	CommentDetails detail = new CommentDetails();
            detail.setID(UUID.randomUUID().toString());
            detail.setID_Parent(11);
            detail.setComment(comment);
            detail.setCoomentChild(comment);
            CommentDetails detail1 = new CommentDetails();
            detail1.setID(UUID.randomUUID().toString());
            detail1.setID_Parent(11);
            Comment commentCom = this.findComment(commentID);
            detail1.setComment(commentCom);
            detail1.setCoomentChild(comment);
            
            this.sessionFactory.getCurrentSession().persist(detail); 
            this.sessionFactory.getCurrentSession().persist(detail1);
	}
	
	public void saveAnswerChild(CommentInfo commentInfo){
		Long code = commentInfo.getID();
		String postID = commentInfo.getPostID();
		Long commentID = commentInfo.getCommentID();
		Long commentChildID = commentInfo.getCommentChildID();
		Post post = postDAO.findPost(postID);
        Comment comment = null;
        
        if (code != null) {
        	comment = this.findComment(code);
        }
        if (comment == null) {        	
            
        	comment = new Comment();
            
        	comment.setID(commentInfo.getID());
        	comment.setCreteDate(commentInfo.getCreteDate()); 
        	comment.setLevel(commentInfo.getLevel());
        	comment.setUserName(commentInfo.getUserName());
        	comment.setPost(post);        	
        }  
        comment.setDescription(commentInfo.getDescription());
        
        this.sessionFactory.getCurrentSession().persist(comment);
        
        	CommentDetails detail = new CommentDetails();
            detail.setID(UUID.randomUUID().toString());
            detail.setID_Parent(11);
            detail.setComment(comment);
            detail.setCoomentChild(comment);
            CommentDetails detail1 = new CommentDetails();
            detail1.setID(UUID.randomUUID().toString());
            detail1.setID_Parent(11);
            Comment commentCom = this.findComment(commentID);
            detail1.setComment(commentCom);
            detail1.setCoomentChild(comment);
            CommentDetails detail2 = new CommentDetails();
            detail2.setID(UUID.randomUUID().toString());
            detail2.setID_Parent(11);
            Comment commentCh = this.findComment(commentChildID);
            detail2.setComment(commentCh);
            detail2.setCoomentChild(comment);
            this.sessionFactory.getCurrentSession().persist(detail); 
            this.sessionFactory.getCurrentSession().persist(detail1);
            this.sessionFactory.getCurrentSession().persist(detail2);
	}
	
	public void remove(Long commentID) {
		Comment comment = this.findComment(commentID);
		sessionFactory.getCurrentSession().delete(comment);
	}
}


