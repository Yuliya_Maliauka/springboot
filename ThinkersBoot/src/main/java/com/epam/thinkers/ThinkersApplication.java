package com.epam.thinkers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class ThinkersApplication {

	 
	   	
	public static void main(String[] args) {
		SpringApplication.run(ThinkersApplication.class, args);
	}
}
