package com.epam.thinkers.model;

import java.util.Date;

public class PostDetaisInfo {

	private String id;
	private String postName;
	private String postDescription;
	private String postUser;
	private Date postDate;
	private String topicID;	
	private String topicName;	
	private String topicDescription;	
	private String topicUser;		
	private boolean topicFlag;
	public PostDetaisInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PostDetaisInfo(String id, String postName, Date postDate, String postUser, String postDescription, String topicName, String topicID){
		this.id = id;
		this.postName = postName;
		this.postDate = postDate;
		this.postUser = postUser;
		this.postDescription = postDescription;	
		this.topicName = topicName;
		this.topicID = topicID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public String getPostDescription() {
		return postDescription;
	}

	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}

	public String getPostUser() {
		return postUser;
	}

	public void setPostUser(String postUser) {
		this.postUser = postUser;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public String getTopicID() {
		return topicID;
	}

	public void setTopicID(String topicID) {
		this.topicID = topicID;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicDescription() {
		return topicDescription;
	}

	public void setTopicDescription(String topicDescription) {
		this.topicDescription = topicDescription;
	}

	public String getTopicUser() {
		return topicUser;
	}

	public void setTopicUser(String topicUser) {
		this.topicUser = topicUser;
	}

	public boolean isTopicFlag() {
		return topicFlag;
	}

	public void setTopicFlag(boolean topicFlag) {
		this.topicFlag = topicFlag;
	}
	
	
}
