package com.epam.thinkers.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.epam.thinkers.entity.Post;

public class CommentInfo {

	private Long ID;
	private String userName;
	private String description;
	private Date creteDate;
	private int level;
	private String postID;
	private Long commentID;
	private Long commentChildID;
	
	private CommentInfo comment;
	private final List<CommentInfo> commentLines = new ArrayList<CommentInfo>();
	
	private List<CommentDetailsInfo> commentDetails;
	
	private boolean newComment=false;
	
	public CommentInfo() {

	}

	public CommentInfo(Long iD, String userName, String description, Date creteDate, int level, String postID) {
		ID = iD;
		this.userName = userName;
		this.description = description;
		this.creteDate = creteDate;
		this.level = level;
		this.postID = postID;
	}

	public String getPostID() {
		return postID;
	}

	public void setPostID(String postID) {
		this.postID = postID;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Long getCommentID() {
		return commentID;
	}

	public void setCommentID(Long commentID) {
		this.commentID = commentID;
	}

	
	public Long getCommentChildID() {
		return commentChildID;
	}

	public void setCommentChildID(Long commentChildID) {
		this.commentChildID = commentChildID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreteDate() {
		return creteDate;
	}

	public void setCreteDate(Date creteDate) {
		this.creteDate = creteDate;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public CommentInfo getComment() {
		return comment;
	}

	public void setComment(CommentInfo comment) {
		this.comment = comment;
	}

	public List<CommentDetailsInfo> getCommentDetails() {
		return commentDetails;
	}

	public void setCommentDetails(List<CommentDetailsInfo> commentDetails) {
		this.commentDetails = commentDetails;
	}

	public boolean isNewComment() {
		return newComment;
	}

	public void setNewComment(boolean newComment) {
		this.newComment = newComment;
	}

	public List<CommentInfo> getCommentLines() {
		return commentLines;
	}

	private CommentInfo findCommentLineById(Long id) {
        for (CommentInfo line : this.commentLines) {
            if (line.getID() == id) {
                return line;
            }
        }
        return null;
    }
 
    public void addComment(CommentInfo commentInfo) {
    	
            this.commentLines.add(commentInfo);    	 
    }
    
    public void removeComment(Long id) { 
    	CommentInfo line = this.findCommentLineById(id);
    	if (line != null) {
            this.commentLines.remove(line);            
        }   
    }
 
    public boolean isEmptyComments() {
        return this.commentLines.isEmpty();
    }
    

}
