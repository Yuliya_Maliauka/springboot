package com.epam.thinkers.model;

import java.util.Date;

public class CommentDetailsInfo {

	
	private String postId;
	private String postName;
	private String postDescription;
	private String postUser;
	private Date postDate;
	
	private String ID;
	
	private Long ID_comment;
	private String userName;
	private String description;
	private Date creteDate;
	private int level;
	
	
	private Long ID_coomentChild;
	private String userNameChild;
	private String descriptionChild;
	private Date creteDateChild;
	private int levelChild;
	
	private int ID_Parent;
		
	public CommentDetailsInfo() {

	}
	
	public CommentDetailsInfo(String iD,String postId, Long iD_comment, Long iD_coomentChild, String userNameChild, String descriptionChild, Date creteDateChild, int levelChild,
			int iD_commentParent) {
		
		ID = iD;		
		this.postId = postId;
		ID_comment = iD_comment;
		ID_coomentChild = iD_coomentChild;
		this.userNameChild = userNameChild;
		this.descriptionChild = descriptionChild;
		this.creteDateChild = creteDateChild;
		this.levelChild = levelChild;
		ID_Parent = iD_commentParent;
	}
	public CommentDetailsInfo(String iD, String postId, Long iD_comment, String userName, String description, Date creteDate, int level) {
		
		ID = iD;		
		this.postId = postId;
		ID_comment = iD_comment;
		this.userName = userName;
		this.description = description;
		this.creteDate = creteDate;
		this.level = level;
	}

	public String getUserNameChild() {
		return userNameChild;
	}


	public void setUserNameChild(String userNameChild) {
		this.userNameChild = userNameChild;
	}


	public String getDescriptionChild() {
		return descriptionChild;
	}


	public void setDescriptionChild(String descriptionChild) {
		this.descriptionChild = descriptionChild;
	}


	public Date getCreteDateChild() {
		return creteDateChild;
	}


	public void setCreteDateChild(Date creteDateChild) {
		this.creteDateChild = creteDateChild;
	}


	public int getLevelChild() {
		return levelChild;
	}


	public void setLevelChild(int levelChild) {
		this.levelChild = levelChild;
	}


	public String getID() {
		return ID;
	}


	public void setID(String iD) {
		ID = iD;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getCreteDate() {
		return creteDate;
	}


	public void setCreteDate(Date creteDate) {
		this.creteDate = creteDate;
	}


	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public Long getID_comment() {
		return ID_comment;
	}


	public void setID_comment(Long iD_comment) {
		ID_comment = iD_comment;
	}


	public Long getID_coomentChild() {
		return ID_coomentChild;
	}


	public void setID_coomentChild(Long iD_coomentChild) {
		ID_coomentChild = iD_coomentChild;
	}


	public int getID_commentParent() {
		return ID_Parent;
	}


	public void setID_commentParent(int iD_commentParent) {
		ID_Parent = iD_commentParent;
	}


	public String getPostId() {
		return postId;
	}


	public void setPostId(String postId) {
		this.postId = postId;
	}


	public String getPostName() {
		return postName;
	}


	public void setPostName(String postName) {
		this.postName = postName;
	}


	public String getPostDescription() {
		return postDescription;
	}


	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}


	public String getPostUser() {
		return postUser;
	}


	public void setPostUser(String postUser) {
		this.postUser = postUser;
	}


	public Date getPostDate() {
		return postDate;
	}


	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	
	
}
