package com.epam.thinkers.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "topic_account")
public class TopicDetails implements Serializable {
 
    private static final long serialVersionUID = 7550745928843183535L;
 
    private String id;
    private Account user;
    private Topic topic;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "User_Name", nullable = false, //
    foreignKey = @ForeignKey(name = "TOPIC_ACCOUNT_USER_FK") )
    public Account getUser() {
		return user;
	}
	public void setUser(Account user) {
		this.user = user;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Topic_ID", nullable = false, //
    foreignKey = @ForeignKey(name = "TOPIC_ACCOUNT_TOP_FK") )
	public Topic getTopic() {
		return topic;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	
	@Id
    @Column(name = "ID", length = 50, nullable = false)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
  
}
