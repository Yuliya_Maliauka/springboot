package com.epam.thinkers.entity;

import static javax.persistence.GenerationType.IDENTITY;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "comment")
public class Comment implements Serializable {
 
    private static final long serialVersionUID = -5896670215452363100L;
 
	private Long ID;
	private String userName;
	private String description;
	private Date creteDate;
	private int level;
	private Post post;
	
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID")
	public Long getID() {
		return ID;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	
	@Column(name = "User_Name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Date_Create")
	public Date getCreteDate() {
		return creteDate;
	}
	public void setCreteDate(Date creteDate) {
		this.creteDate = creteDate;
	}
	
	@Column(name = "Level")
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Post_ID", //
    foreignKey = @ForeignKey(name = "FK_COMMENT_POST") )
	public Post getPost() {
		return post;
	}
	public void setPost(Post post) {
		this.post = post;
	}
	
}
