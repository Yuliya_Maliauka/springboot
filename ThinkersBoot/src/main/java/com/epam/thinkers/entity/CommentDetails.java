package com.epam.thinkers.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "set_comment")
public class CommentDetails implements Serializable {
 
    private static final long serialVersionUID = 965474596958183535L;
 
    private String ID;
	private Comment comment;
	private Comment coomentChild;
	private int ID_Parent;
	
	
	@Id
    @Column(name = "ID")
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_Comment", //
    foreignKey = @ForeignKey(name = "FK_SET_COMMENT_1") )
	public Comment getComment() {
		return comment;
	}
	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_Comment_Child", //
    foreignKey = @ForeignKey(name = "FK_SET_COMMENT_2") )
	public Comment getCoomentChild() {
		return coomentChild;
	}
	public void setCoomentChild(Comment coomentChild) {
		this.coomentChild = coomentChild;
	}
	
	@Column(name = "ID_Parent")
	public int getID_Parent() {
		return ID_Parent;
	}
	public void setID_Parent(int iD_Parent) {
		ID_Parent = iD_Parent;
	}
	
	
}
