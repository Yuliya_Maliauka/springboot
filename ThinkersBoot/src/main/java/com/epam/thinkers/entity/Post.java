package com.epam.thinkers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "posts")
public class Post implements Serializable {
 
    private static final long serialVersionUID = -1000119078147252957L;
 
    private String id;
    private String namePost;
    private Date dateCreate;
    private String description;
    private String creatorUser;
    private Topic topic;
    
	
    @Id
    @Column(name = "ID")
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "User_Name")
	public String getCreatorUser() {
		return creatorUser;
	}
	public void setCreatorUser(String creatorUser) {
		this.creatorUser = creatorUser;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Topic_ID", nullable = false, //
    foreignKey = @ForeignKey(name = "TOPIC_POST_FK") )
	public Topic getTopic() {
		return topic;
	}
	
	public void setTopic(Topic topic) {
		this.topic = topic;
	} 
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Date_Create")
	public Date getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}
	
	@Column(name = "Post_Name", nullable = false)
	public String getNamePost() {
		return namePost;
	}
	public void setNamePost(String namePost) {
		this.namePost = namePost;
	}
	
	@Column(name = "Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
    
    
}
